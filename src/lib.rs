#![warn(clippy::all, clippy::pedantic)]
//! Small library for opening a uri with an appropriate program.
//! ## Usage
//! ```Rust
//! use mime_open::open
//!
//! if let Err(e) = open("https://duckduckgo.com") {
//!     eprintln!("Error opening url: {}", e);
//! }
//! ```

#[cfg(all(target_family = "unix", not(target_os = "macos")))]
mod unix;
#[cfg(all(target_family = "unix", not(target_os = "macos")))]
pub use unix::open;

#[cfg(target_os = "macos")]
mod macos;
#[cfg(target_os = "macos")]
pub use macos::open;

#[cfg(target_os = "windows")]
mod windows;
#[cfg(target_os = "windows")]
pub use windows::open;
