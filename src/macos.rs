use std::error::Error;
use std::process::Command;

pub fn open(uri: &str) -> Result<(), Box<dyn Error>> {
    Command::new("open").arg(uri).spawn()?;
    Ok(())
}
