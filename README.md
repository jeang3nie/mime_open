Small library for opening a uri with an appropriate program.
## Usage
```Rust
use mime_open::open

if let Err(e) = open("https://duckduckgo.com") {
    eprintln!("Error opening url: {}", e);
}
```
